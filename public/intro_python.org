#+TITLE: Algoritmos e introdução ao Python
#+AUTHOR: Pedro Belin Castellucci
#+LANGUAGE: br
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="./myorgstyle.css"/>

* Algoritmos e introdução ao Python

Um algoritmo pode ser definido como uma sequência de passos finita e
bem definida para a solução de um problema de forma eficiente.

De acordo com a definição anterior, a sequência de passos deve estar
*bem definida*. Contudo, isso pode depender do contexto. Veja, por
exemplo, o vídeo a seguir.

#+begin_export html
<iframe width="560" height="315" src="https://www.youtube.com/embed/pdhqwbUWf4U?si=CBU7DH87C95yo2xM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
#+end_export

No contexto de programação, uma sequência bem definida implica que as
 instruções que compõem um algoritmo podem ser executadas por um
 computador. 

Há diversas formas de representar um algoritmo:

  * Uma descrição textual.
  * Graficamente, como em um fluxograma.
  * Utilizando pseudo-código.

** Um exemplo -- Verificando o critério de aprovação

Um estudante é aprovado em uma disciplina de acordo com sua nota
final, que é calculada segundo a expressão

$$MF = \frac{4P_1 + 4P_2 + 2EX}{10}$$

A nota mínima para aprovação na disciplina deve ser $MF\geq 6$ (seis).
O estudante com $3{,}0 \leq MF < 6{,}0$ terá direito a uma nova
avaliação ao final do semestre (REC), sendo a nota final (NF)
calculada como $NF = \frac{MF + REC}{2}$.

A descrição textual anterior pode ser apresentada utilizando um
fluxograma.

#+BEGIN_SRC dot :file images/fluxograma_media.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
  digraph grade {
    start [shape=doublecircle, label=""];
    init [label="MF = 0.4P1 + 0.4P2 + 0.2EX", shape=box];
    check_approved [label="MF >= 6?", shape=diamond];

    check_rec [label="3 <= MF < 6", shape=diamond];
    rec_grade [label="NF = 0.5(MF + REC)", shape=box];
    check_rec_approved [label="NF >= 6?", shape=diamond];
    approved [label="Aprovado", shape=box];
    failed [label="Reprovado", shape=box];

    start -> init;
    init -> check_approved;
    check_approved -> approved [label="Sim"];
    check_approved -> check_rec [label="Não"];
    check_rec -> rec_grade[label="Sim"];
    check_rec -> failed[label="Não"];
    rec_grade -> check_rec_approved;
    check_rec_approved -> approved [label="Sim"];
    check_rec_approved -> failed [label="Não"];
  }
#+END_SRC

#+attr_html: :height 400px
#+RESULTS:
[[file:images/fluxograma_media.png]]

** Outro exemplo -- Encontrado a carta 

Você recebeu uma pilha de cartas numeradas e deseja encontrar uma
carta-alvo (p.e. 5 de Ouros). A seguir é apresentado um fluxograma representando um
algoritmo para identificar a posição da carta-alvo na pilha (primeiro,
segundo, terceiro etc). Suponha que a carta está presente.

Para aumentar a expressividade podemos definir a seguinte notação.
 * $P$: pilha de cartas.
 * $P.pop()$: revela e remove a carta do topo do pilha.
 * $pos$: variável que armazena a posição da carta na pilha.
 * $x$: representa a carta-alvo.


#+BEGIN_SRC dot :file images/fluxograma_encontrar_carta.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
  digraph card {
    start [shape=doublecircle, label=""];
    init [label="pos = 1", shape=box];
    check_found [label="x é igual a P.pop()?", shape=diamond];
    found_pos [label="Responda 'pos'", shape=box];
    update [label="Atualize 'pos' para 'pos+1'", shape=box];

    start -> init;
    init -> check_found;
    check_found -> update [label="Não"];
    check_found -> found_pos [label="Sim"];
    update -> check_found;
  }
#+END_SRC

#+RESULTS:
[[file:images/fluxograma_encontrar_carta.png]]


As estruturas que apareceram nos dois exemplos são importantes
elementos para a construção de algoritmos:

 * Estrutura sequencial: A execução de um passo (instrução) após o outro.
 * Estrutura condicional: A execução de um ramo o outro de instruções
   baseados em uma condição (representada no fluxograma pelo losango).
 * Estrutura de repetição: A execução iterativa de um ou mais comandos
   repetidamente, chamado laço de repetição.

* Olá, mundo!

Abra um editor de sua preferência (Thonny, VSCode, Spider, Eclipse,
https://www.online-python.com/ ou outro) e digite o seguinte código.

#+begin_src python
  print("Hello, World!")
#+end_src

#+RESULTS:
: None

Salve o arquivo com o nome =hello_world.py=. A extensão =.py= é importante
para que seja reconhecido que o arquivo contém um código em Python.

Execute o arquivo. Parabéns! Você programou e executou o seu primeiro
programa em Python.

Agora, seguem algumas observações:

 + =print= é uma função do Python. Representa um
   comando de saída do programa.   
 + Funções são identificadas por serem seguidas de parênteses. 
 + Dentro dos parênteses pode haver uma lista de argumentos, separados
   por vírgula.
 + Um dos argumentos possíveis para a função =print= é um texto (cadeia
   de caracteres) que será impresso na tela do usuário. 
 + Uma cadeia de caracteres deve ser envolvida em aspas (simples ou duplas).

** Utilizando a entrada

Modifique o código para o seguinte:

#+begin_src python
  nome = input("Qual é o seu nome?")
  print("Oi ", nome)
#+end_src

 + A função =input= é um função do Python que permite ler a entrada do teclado.
 + A função =input= *retorna* a sequência de caracteres que for lida do teclado.
 + Esse código também usa um comando de atribuição ($=$) e uma variável chamada =nome= recebe o retorno da função =input=.

* Variáveis, tipos de dados e atribuição

Para manipular informações o computador precisa armazená-las em
memória. Uma variável é um nome (uma "etiqueta'') de um espaço de
memória que armazena um determinado *tipo de dado*.

Um *tipo de dado* é uma forma de classificar a informação que está
armazenada na memória. Com isso, é possível dizer como aquele dado
deve ser manipulado.

Alguns dos tipos de dados embutidos do Python são apresentados a
seguir (uma lista completa pode ser vista [[https://www.w3schools.com/python/python_datatypes.asp][neste link]]).

#+BEGIN_SRC dot :file images/python_data_types.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
  digraph data_types {
	  rankdir=TB;
	  text	 [shape=box, label="Textuais"]; 
	  str [shape=plaintext];

	  numeric	 [shape=box, label="Numéricos"];
	  int [shape=plaintext];
	  float [shape=plaintext];
	  complex [shape=plaintext];

	  sequences[shape=box, label="Sequências"];
	  list [shape=plaintext];
	  tuple [shape=plaintext];
	  range [shape=plaintext];

	  boolean	 [shape=box, label="Booleanos"];
	  bool [shape=plaintext];

	  text -> str;
	  numeric -> int;
	  numeric -> float;
	  numeric -> complex;
	  sequences -> list;
	  sequences -> tuple;
	  sequences -> range;
	  boolean -> bool;
  }
#+END_SRC

#+attr_html: :width 800px
#+RESULTS:
[[file:images/python_data_types.png]]

** Cadeias de caracteres (/strings/)

/Strings/ são sequências de caracteres. Com frequência são utilizadas
para representar uma informação textual, como uma mensagem a ser
impressa na tela do usuário. Em Python, uma /string/ deve ser limitada
com aspas (duplas ou simples). Alguns exemplos de /strings/ são dados
a seguir (o primeiro representa uma /string/ vazia).

#+begin_src python
  ""
  "     "
  "Olá, Mundo"
  'Pode ser aspas simples'
  '$!@$'
#+end_src

Diversas operações são permitidas com /string/ como, por exemplo, a
concatenação (junção). A operação de concatenação é representada pelo
símbolo $+$.

#+begin_exercise
Adapte o código a seguir para pedir o nome e, depois, o sobrenome do
usuário. Armazene o nome e sobrenome em variáveis separadas e imprima
a mensagem "Oi, " seguida do nome e sobrenome. A mensagem deve ser
impressa toda na mesma linha. Utilize a operação de concatenação.o

#+begin_src python
  nome = input("Qual é o seu nome?")
  print("Oi, ", nome)
#+end_src
#+end_exercise

Outra função que pode ser aplicada a uma /string/ é a função =len= que
retorna o número de caracteres de uma /string/.

#+begin_exercise
Faça um programa que pede uma frase $F$ ao usuário e imprime a mensagem
"Sua frase tem {n} caracteres " em que $n$ é o número de caracteres de
$F$.

- Dica: Para imprimir a mensagem você pode utilizar =print(f"Sua frase
  tem {n} caracteres")= em que =n= armazena comprimento da frase.
#+end_exercise

** Tipos numéricos e operações aritméticas

Em Python os tipos numéricos são

 * =int=: para representação de números inteiros.
 * =float=: para representaçãode números em ponto-flutuante ("fracionários").
 * =complex=: para a representação de números complexos. 

Os operadores aritméticos em Python são:

|   <c>    |            <c>            |
| Operador |           Nome            |
|----------+---------------------------|
|    +     |          Adição           |
|    -     |        Substração         |
|    *     |       Multiplicação       |
|    ​/     |          Divisão          |
|    //    |      Divisão inteira      |
|    %     | Módulo (resto da divisão) |
|    **    |       Exponenciação       |

Cada operador tem o mesmo sentido da Matemática tradicional. 

#+begin_highlight
É importante estar atento ao tipo de dado que cada variável armazena. 
Por exemplo, o símbolo $+$ também é utilizado para concatenar /strings/. O mesmo
símbolo pode ser utilizado para mais de uma operação, isso é possível
pois o tipo dos dados envolvidos definem o comportamento da operação.
#+end_highlight

#+begin_exercise
A operação de adição para números reais é comutativa, isto é, $a +
b = b + a$. Prove que
o operador '+' para /strings/ não é comutativo.
#+end_exercise


#+begin_exercise
O que deve imprimir o seguinte código, execute-o e confirme sua hipótese
#+begin_src python
  x = input("Digite um número: ")
  print("O dobro do seu número é ", x + x)
#+end_src
#+end_exercise

#+begin_highlight
A função =input=, utilizada para ler informações do teclado, retorna
uma /string/. É importante converter a informação lida para o tipo de
dado desejado.
#+end_highlight

Para ler um número inteiro, pode-se combinar a função =input= com a
função =int=:

#+begin_src python
  x = int(input("Digite um número:o "))
#+end_src

Analogamente, para ler um número fracionário, pode-se utilizar a
função =float=.

** Atribuição de variáveis

O comando de atribuição de variáveis === faz com que o conteúdo do
lado direito seja armazenado na posição de memória indicada pelo lado
esquerdo do operador. Note, então, que o lado direito deve ser
avaliado, buscando na memória todos os valores correspondentes e
realizado as operações necessárias. Por exemplo, considere o código a
seguir. 

#+begin_src python
  a = 2
  b = 3
  c = a**2 + b**3
  print(c)
#+end_src

Ele define três variáveis =a=, =b= e =c=, isto é, define três espaços (posições)
de memória. Na primeira linha de código, o valor =2= é armazenado na
posição =a=. Em seguida, o valor =3= é armazenado na posição =b=,
então, o valor =c= armazena o resultado da expressão =a**2 + b**3=, ou
seja, =31=, que é, por fim, impresso na tela. 

Perceba que caso a terceira linha fosse alterada para o seguinte, o
resultado seria o mesmo. 
#+begin_src python
  a = 2
  b = 3
  a = a**2 + b**3
  print(a)
#+end_src

A semântica do símbolo "===" em Python (e diversas outras linguagens de
programação) *não* deve ser confundida com a semântica matemática de
igualdade. Inclusive, por isso, muitos pseudocódigos utilizam o
símbolo $\leftarrow$ para indicar atribuição. 

#+begin_highlight
Um dos objetivos de uma descrição de algoritmo em pseudocódigo é
evitar a dependência de uma linguagem de programação específica e, ao
mesmo tempo, manter os elementos
básicos da programação imperativa (expressões, condições, laços etc).
#+end_highlight

Em pseudocódigo, o código anterior poderia ser escrito da seguinte
forma:
\begin{align*}
&a \leftarrow 2\\
&b \leftarrow 3\\
&c \leftarrow a^{2} + b^{3}\\
&Imprima (a)
\end{align*}

*** Nomeação de variáveis

Há algumas regras (e algumas boas práticas) para nomear variáveis. As
regras são:

 + o nome deve começar com uma letra ou o símbolo de sublinhado "=_=";
 + o nome não deve começar com um número;
 + o nome deve conter apenas caracteres alfa-numéricos (letras e
   números) ou sublinhado;
 + os nomes de variáveis são /case-sensitive/, isto é, diferenciam
   maiúsclas e minúsculas.

Como uma boa prática é importante escolher nomes expressivos. A não
ser em casos especiais, os nomes de variáveis tipicamente iniciam com
letra minúscula.


* Referências e outros materiais

+ [[https://edisciplinas.usp.br/course/view.php?id=107192][Material da professora Dra Marina Andretta]].
+ https://www.w3schools.com/python/
